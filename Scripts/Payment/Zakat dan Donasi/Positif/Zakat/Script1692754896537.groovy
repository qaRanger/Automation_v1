import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.delay(7)

Mobile.tap(findTestObject('Menu Utama/Icon_Payment'), 1)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Menu Payment/Icon_ZakatandDonation'), 1)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Menu Zakat dan Donasi/Area_Header_ZakatDonation'), 2)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_PaymentService'), 1)

def serviceTypeZakat = Mobile.getAttribute(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Service/Button_Zakat'),'content-desc', 1)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Service/Button_Zakat'), 1)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_PaymentAgency'), 1)

def paymentAgency = Mobile.getText(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Agency/Field_YayasanRumahZakatIndonesia'), 2)
Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Agency/Field_YayasanRumahZakatIndonesia'), 2)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_Amount'),1)

Mobile.sendKeys(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_Amount'), ZakatAmt )

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_Notes'),1)
Mobile.setText(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_Notes'), Notes, 1)
Mobile.hideKeyboard()
Mobile.delay(1)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Field_SelectSourceAccount'),2 )

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.70
Mobile.swipe(startX, endY, endX, startY)

def getcontentdesc = Mobile.getAttribute(findTestObject('Object Repository/Menu Zakat dan Donasi/Select Source Account/SOF_Syariah'), 'content-desc', 1)
def sourceAccount = getcontentdesc.replaceAll("[*]", "")

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Select Source Account/SOF_Syariah'), 1)

Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Btn_Next'), 4)

Mobile.delay(1)

// Cek Halaman Confirmation

Mobile.delay(2)
Mobile.verifyElementText(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_ServiceType'), serviceTypeZakat)


Mobile.verifyElementExist(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_Company'), 1)
Mobile.verifyElementText(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_StringCompanyName'), paymentAgency)

Mobile.verifyElementVisible(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_StrNote'), 0)
//Mobile.verifyElementExist(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_StrNote'), 0)
Mobile.verifyElementText(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Text_StrNote'), Notes)



// - Cek Halaman Confirmation
Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Confirmation/Btn_Pay'), 1)
Mobile.delay(1)

for(int i = 1; i < 7; i++) {
	Mobile.tap(findTestObject('Object Repository/Menu Zakat dan Donasi/Enter Pin/Numpad_1'), 1)
}

Mobile.delay(5)

Mobile.waitForElementPresent(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Success/Text_PaymentSuccess'), 2)

Mobile.verifyElementExist(findTestObject('Object Repository/Menu Zakat dan Donasi/Payment Success/Text_PaymentSuccess'), 3)

Mobile.delay(5)