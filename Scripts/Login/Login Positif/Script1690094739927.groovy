import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.builtin.CheckElementKeyword as CheckElementKeyword
import com.kms.katalon.core.mobile.keyword.builtin.VerifyElementExistKeyword as VerifyElementExistKeyword
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.ui.SystemOutputInterceptor as SystemOutputInterceptor
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.G_AppPath, false)

Mobile.delay(7, FailureHandling.STOP_ON_FAILURE)

Mobile.comment('Tap Object biar next login Field USERIDnya kosong')

Mobile.tap(findTestObject('Object Repository/Menu Login/Clickable_Text_SaveUserID'),1)

Mobile.tap(findTestObject('Menu Login/Input_UserID'), 2)

Mobile.setText(findTestObject('Menu Login/Input_UserID'), USERID1, 1, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Menu Login/Input_Password'), 2)

Mobile.setText(findTestObject('Menu Login/Input_Password'), Password, 1)

Mobile.hideKeyboard()

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Menu Login/Button_Login_FieldIsFilled'), 0)

Mobile.delay(4)

Mobile.waitForElementPresent(findTestObject('Object Repository/Menu Utama/Text_AccountName'), 0)

