import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.G_AppPath, false)

Mobile.delay(7, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Menu Login/Input_UserID'), 2)

Mobile.setText(findTestObject('Menu Login/Input_UserID'), USERID1, 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Menu Login/Input_Password'), 2)

Mobile.setText(findTestObject('Menu Login/Input_Password'), Password, 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

if(CASEID.equals('1')) {
	Mobile.delay(2)
	Mobile.waitForElementPresent(findTestObject('Object Repository/Menu Login/Button_Login'), 0)
}
if(CASEID.equals('2')) {
	Mobile.delay(2)
	Mobile.tap(findTestObject('Menu Login/Button_Login_FieldIsFilled'), 0)
	Mobile.verifyElementExist(findTestObject('Menu Login/SubFolder_ErrorMessage/Error_DataNotFound'), 0)
}


Mobile.tap(findTestObject('Menu Login/Button_Login_FieldIsFilled'), 0)

Mobile.verifyElementExist(findTestObject('Menu Login/SubFolder_ErrorMessage/Error_DataNotFound'), 0)

Mobile.verifyElementExist(findTestObject(null), 0)

Mobile.waitForElementPresent(findTestObject('Object Repository/Menu Utama/Text_AccountName'), 0)

